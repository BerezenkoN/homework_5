package ManStudent;

/**
 * Created by user on 16.10.2016.
 */
public class Student extends Man {
    @Override

    public String toString() {

        return String.format("Student: {name: %s, age: %d, sex: %b, weight: %d, course %d}", name, age, sex, weight, StudyYear);

    }
    private int StudyYear;



    public int getStudyYear() {

        return StudyYear;
    }



    public void setStudyYear(int StudyYear) {

        if (StudyYear < 0) throw new IllegalArgumentException("StudyYear can't be negative");

        this.StudyYear = StudyYear;

    }



    public Student(String name, int age, boolean sex, int weight, int StudyYear) {

        super(name, age, sex, weight);

        setStudyYear(StudyYear);

    }


    public Student() {

        setStudyYear(1);

    }


    public int incStudyYear(int delta) {

        if (delta < 0) throw new IllegalArgumentException("Wrong increment");

        StudyYear += delta;

        return StudyYear;

    }




}

