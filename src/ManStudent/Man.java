package ManStudent;

/**
 * Created by user on 16.10.2016.
 */
public class Man {
    protected String name;
    protected boolean sex;
    protected int age;
    protected int weight;

    public Man() {
    }


    public String getName() {

        return name;

    }



    public void setName(String name) {

        if (name == null && name == "") throw new IllegalArgumentException("Incorrect name");

        this.name = name;

    }



    public int getAge() {

        return age;

    }




    public String toString() {

        return String.format("Man: {name: %s, sex: %b, age:%d, weight: %d}", name, sex, age,  weight);

    }



    public void setAge(int age) {

        if (age < 0) throw new IllegalArgumentException("Age can't be negative");

        this.age = age;

    }



    public int getWeight() {

        return weight;

    }



    public void setWeight(int weight) {

        if (weight < 0) throw new IllegalArgumentException("Weight can't be negative");

        this.weight = weight;

    }


    public Man(String name, int age, boolean sex, int weight) {

        setAge(age);

        setName(name);

        setWeight(weight);

        this.sex = sex;

    }
}


