package Matrix;
import java.util.Random;

/**
 * Created by user on 16.10.2016.
 */
public class Matrix {
    private int[][] matrix;
    private int height;
    private int width;

    public Matrix(int width, int height) {
        this.width = width;
        this.height = height;
        this.matrix = new int[width][height];
        Random rnd = new Random();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                this.matrix[i][j] = rnd.nextInt();
            }
        }
    }

    public Matrix(int[][] matrix) {
        this.width = matrix.length;
        this.height = matrix[0].length;
        this.matrix = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }

    }
    public int[][] getMatrix() {
        return this.matrix;
    }

    public void setMatrix(int[][] Matrix) {
        if (matrix == null) throw new IllegalArgumentException();
        this.width = matrix.length;
        this.height = matrix[0].length;
        this.matrix = matrix;
    }

    public int[][] sumWithMatrix(Matrix anotherMatrix) throws IllegalArgumentException {
        int[][] result = new int[width][width];
        if ((width != anotherMatrix.getMatrix().length) || (width != anotherMatrix.getMatrix()[0].length))
            throw new IllegalArgumentException("Wrong size of the given matrix");
        else {
            for (int i = 0; i < width; i++)
                for (int j = 0; j < width; j++)
                    result[i][j] = this.matrix[i][j] + anotherMatrix.getMatrix()[i][j];

        }

        return result;
    }

    public int[][] multiplyMatrix(int n) {
        int[][] result = new int[width][width];
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++) result[i][j] = this.matrix[i][j] * n;
        return result;


    }

    public int[][] multiplyMatrix(int[][] anotherMatrix) throws IllegalArgumentException {
        int[][] result = new int[width][anotherMatrix[0].length];
        if ((width != anotherMatrix.length)) throw new IllegalArgumentException("Wrong size of the given matrix");
        else {
            for (int i = 0; i < width; i++)
                for (int j = 0; j < anotherMatrix[0].length; j++) {
                    for (int z = 0; z < width; z++)
                        result[i][j] += this.matrix[i][z] * anotherMatrix[z][j];
                }
        }
        return result;
    }

    public int[][] multiplyMatrix(Matrix anotherMatrix) throws IllegalArgumentException {
        int[][] result = new int[width][anotherMatrix.getMatrix()[0].length];
        if ((width != anotherMatrix.getMatrix()[0].length))
            throw new IllegalArgumentException("Wrong size of the given matrix");
        else {
            for (int i = 0; i < width; i++)
                for (int j = 0; j < anotherMatrix.getMatrix()[0].length;) {
                    for (int z = 0; z < width; z++)
                        result[i][j] += this.matrix[i][z] * anotherMatrix.getMatrix()[z][j];
                }
        }
        return result;
    }

    public int[][] transpose() {
        int[][] result = new int[width][width];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < width; j++)
                result[i][j] = this.matrix[j][i];
        return result;
    }

    public void prinMatrix() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < width; j++)
                System.out.print(this.matrix[i][j] + " ");
            System.out.println();
        }
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < matrix.length; i++) {

            sb.append("{\t");
            for (int j = 0; j < matrix[0].length; j++) {
                sb.append(matrix[i][j]).append("\t");

            }

            sb.append("}\n");

        }

        return sb.toString();

    }
}
